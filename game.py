from random import randint

# Need an input function to input user name
name = input("Hi! What is your name? ")

# Allow the computer to guess 5 times -> Loop 5 times
for guess in range(5):
    # Need to assign random value to appropriate variables
    m = randint(1,12)
    yyyy = randint(1924,2004)

    # Need an input function of Yes or No after we guess a random month and year thru randint function
    print(f"Guess {guess + 1}: {name} were you born in {m}/{yyyy}?")
    answer = input("yes or no? ")

    # Need conditional statements to see if the guess birthday is correct
    # If not correct, then it needs to guess again until it gets it correct
    if answer == "yes":
        print("I knew it!")
        break

    elif answer == "no" and guess < 4:
        print("Drat! Lemmme try again!")

    elif answer == "no" and guess == 4:
        print("I have other things to do now. Goodbye.")


    # Outlier case if user inputs something other than yes or no.
    else:
        print("Please answer yes or no.")


if answer == "yes":
    print("Thanks for playing!")
