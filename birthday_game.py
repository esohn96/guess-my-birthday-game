from random import randint

# Need an input function to input user name
name = input("Hi! What is your name? ")
later = 2004
earlier = 1924

# Allow the computer to guess 5 times -> Loop 5 times
for guess in range(5):
    # Need to assign random value to appropriate variables
    m = randint(0,11)
    d = randint(1,31)
    yyyy = randint(earlier, later)

    month_list = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"]

    mm = month_list[m]

    # Need an input function of Yes or No after we guess a random month and year thru randint function
    print(f"Guess {guess + 1}: {name} were you born in {mm}/{d}/{yyyy}?")
    answer = input("yes or earlier or later? ")

    # Need conditional statements to see if the guess birthday is correct
    # If not correct, then it needs to guess again until it gets it correct
    if answer == "yes":
        print("I knew it!")
        break

    elif answer == "earlier" and guess < 4:
        print("Drat! Lemme try again!")
        later = yyyy - 1

    elif answer == "later" and guess < 4:
        print("Drat! Lemme try again!")
        earlier = yyyy + 1

    elif guess == 4:
        print("I have other things to do now. Goodbye.")


    # Outlier case if user inputs something other than yes or no.
    else:
        print("Please answer yes or earlier or later.")


if answer == "yes":
    print("Thanks for playing!")
